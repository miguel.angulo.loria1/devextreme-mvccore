﻿var personIndexUi = new function () {
    this.grid = undefined;

    this.init = function () {
        $("#gridPersonas").dxDataGrid({
            showBorders: true,
            dataSource: {
                store: DevExpress.data.AspNet.createStore({
                    key: "Id",
                    loadUrl: "/Persons/GetPersons"
                })
            },
            columns: [
                "Id", "Name", 
                {
                    dataField: "Birthday",
                    caption: "Fecha Nacimiento",
                    dataType: "date",
                    format: "dd/MM/yyyy"
                }
            ],
            filterRow: { visible: true },
            selection: { mode: "single" },
            loadPanel: { text: "Cargando" },
            paging: { pageSize: 10 },
            pager: {
                infoText: "Pagina {0} de {1} ({2} registros)",
                showInfo: true,
                showNavigationButtons: true,
                showPageSizeSelector: true,
                visible: "auto"
            },
            remoteOperations: true
        });
    };
};

personIndexUi.init();