﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoMvcCore.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;

namespace DemoMvcCore.Controllers
{
    public class PersonsController : Controller
    {
        private readonly DemoContext context;

        public PersonsController(DemoContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetPersons(DataSourceLoadOptions options)
        {
            try
            {
                return Json(DataSourceLoader.Load(
                    context.Persons,
                    options));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //return null;
        }
    }
}